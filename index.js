// console.log("Hello world!");

let titleArr = fetch('https://jsonplaceholder.typicode.com/todos')
.then((response)=> response.json())
.then((data) => data.map((num) => {
            return num.title
        })
    );

console.log(titleArr);


fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response)=> response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response)=> response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

fetch('https://jsonplaceholder.typicode.com/todos', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },

    body: JSON.stringify({
        title: 'Created To Do List Item',
        completed: false,
        userId: 1
    })
})
.then((response)=> response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },

    body: JSON.stringify({
        id: 1,
        title: 'Updated To Do List Item',
        description: 'To update the my to do list with a different data structure',
        status: "Pending",
        dateCompleted: "Pending",
        userId: 1
    })
})
.then((response)=> response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: {
        'Content-Type': 'application/json'
    },

    body: JSON.stringify({
        status: "Complete",
        dateCompleted: "29/11/22",
    })
})
.then((response)=> response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'DELETE'
});